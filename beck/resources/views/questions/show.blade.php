<!DOCTYPE html>

<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <link href="{{ URL::asset('custom.css') }}" rel="stylesheet">
</head>


<form name="user_verification" action="/test" method="POST">

@csrf <!-- {{ csrf_field() }} -->

    <div class="container mt-sm-5 my-1">
        <div class="question ml-sm-5 pl-sm-5 pt-2">
            <div class="py-2 h5"><b>{{ $question->question_content }}</b></div>
            <div class="ml-md-3 ml-sm-3 pl-md-5 pt-sm-0 pt-3" id="options">


     @foreach ($question->answer as $answers)
               
                    @foreach ($answers->score as $scores)
     
                           <input type="radio" class="checkmark" id="radio" name="radio[]" value={{$scores->value}}>
                            <label class="options" for="radio"> {{$answers->answer_content}}</label>
                           
                           
                     @endforeach
                @endforeach


            </div>
            <div class="d-flex align-items-center pt-3">

                </div>
            </div>
        </div>

        <div style="text-align:center"> 
<input type="submit" name="submit" Value="Zatwierdź" class="btn btn-primary" />
</div>
</form>