{{$totalscore = null}}
{{$sumary = ''}}
{{$content = ''}}

<?php
    if(isset($_POST['submit'])){
        if(!empty($_POST['radio'])) {
            foreach($_POST['radio'] as $radio){
                $totalscore += $radio;
            }
        }

        if($totalscore <= 0|| $totalscore <= 11){
            $sumary = "Brak depresji";
            $content = "Prawdopodobnie to tymczasowe pogorszenie nastroju, spowodowane bieżącymi wydarzeniami w Twoim życiu.  Jeśli przykre objawy będą utrzymywać się nadal, wykonaj ten test po 7 dniach i porównaj wyniki czy następuje pogorszenie czy poprawa. ";
        }elseif($totalscore <= 12 || $totalscore <=19){
            $sumary = 'Depresja łagodna';
            $content = 'Wynik w tym przedziale wskazuje na potrzebę udania się do psychologa lub psychoterapeuty w celu dalszej diagnostyki.   Łagodne   objawy   depresyjne   leczone   są   psychoterapią,   bez   konieczności   włączania farmakoterapii. Psycholog/ psychoterapeuta w razie konieczności skieruje Cię do lekarza psychiatry. ';
        }elseif($totalscore <=20 || $totalscore <=25){
            $sumary = 'Depresja umiarkowana';
            $content = 'Punktacja w tym przedziale sugeruje podjęcie szybkich działań i kontakt z psychologiem/psychoterapeutąlub psychiatrą. Istnieje prawdopodobieństwo włączenia leczenia farmakologicznego, przeciwdepresyjnegoprzez psychiatrę. Ważne aby oprócz działań farmakologicznych rozpocząć psychoterapię. To warunkujes kuteczne leczenie depresji.';
        }elseif($totalscore <=26 || $totalscore <= 63){
            $sumary = 'Depresja ciężka';
            $content = 'Konieczne jest udanie się do lekarza psychiatry. To niebezpieczny stan dla zdrowia i życia, głównie gdy pojawiają się myśli samobójcze. Psychoterapia jest bardziej intensywna. W niektórych przypadkach koniczne jest leczenie szpitalne aby nie dopuścić do zagrożenia życia.';
        }
    }






?>

<!doctype html>
<html lang="en">
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

        <link href="{{URL::asset('custom_home.css')}}" rel="stylesheet">

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <title>Quiz</title>
    </head>

    <div class="site-wrapper">

        <div class="site-wrapper-inner">

            <div class="container">

                <div class="masthead clearfix">
                    <div class="container inner">
                    </div>
                </div>

                <div class="inner cover">
                    <h1 class="cover-heading">Uzyskałeś: {{$totalscore}} punkty/ów</h1>
                    <p class="lead">{{$sumary}}</p>
                    <p class="lead">{{$content}}</p>
                    <a href="{{route('home')}}" class="btn btn-lg btn-default">Strona główna</a>
                    </p>
                </div>

            </div>

        </div>

    </div>

