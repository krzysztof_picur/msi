
<!doctype html>
<html lang="en">
<head>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

  <link href="{{URL::asset('custom_home.css')}}" rel="stylesheet">

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <title>Home</title>
</head>

  <div class="site-wrapper">

    <div class="site-wrapper-inner">

      <div class="container">

        <div class="masthead clearfix">
          <div class="container inner">
            <h3 class="masthead-brand"></h3>
            <nav>
              <ul class="nav masthead-nav">
                <li class="active"><a href="https://pl.wikipedia.org/wiki/Skala_depresji_Becka">Dowiedz się więcej</a></li>
              </ul>
            </nav>
          </div>
        </div>

        <div class="inner cover">
          <h1 class="cover-heading">Instrukcja:</h1>
          <p class="lead">W każdym pytaniu wybierz tylko jedną odpowiedź, która najlepiej określa Twoje uczucia podczas ostatnich 7 dni (a nie tylko w dniu dzisiejszym). W przypadku wątpliwości, zadaj sobie pytanie: Która z odpowiedzi jest najbliższa temu co czuję i myślę?</p>
          <p class="lead">
            <a href="{{route('quiz')}}" class="btn btn-lg btn-default">Rozpocznij quiz</a>
          </p>
        </div>

      </div>

    </div>

  </div>
  
