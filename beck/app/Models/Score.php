<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Score extends Model
{

  protected $fillable = [
    'value',
    'answer_id'
  ];

  public function answer()
  {
    //return $this->belongsTo('App\Models\Answer');
    return $this->belongsToMany('App\Models\Answer');

  }


    use HasFactory;
}
