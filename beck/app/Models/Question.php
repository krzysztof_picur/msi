<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{

  protected $fillable = [
    'question_content',
  ];

  public function answer()
  {
      //hasMany to pytanie moze miec wiele odpowiedzi
      return $this->hasMany('App\Models\Answer');
  }

  public function next()
{
    return static::where($this->getKeyName(), '>', $this->getKey())->first();
}

    use HasFactory;
}
