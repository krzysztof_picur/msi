<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{

  protected $fillable = [
    'answer_content',
    'question_id'
  ];

  public function question()
  {
      return $this->belongsTo('App\Models\Question');
  }

  public function score()
  {
    return $this->hasMany('App\Models\Score');
  }

    use HasFactory;
}
