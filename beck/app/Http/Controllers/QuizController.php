<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use App\Models\Answer;
use App\Models\Question;
use App\Models\Score;

class QuizController extends Controller
{
    public function index(Request $request, Collection $collection)
    {
      

      //Zwraca zmiena Question razem z przydzielonymi jej odpowiedziami (relacja)
      $questions = Question::with('answer')->get();
      $answers = Answer::with('score')->get();

      
      // return redirect('home')->withInput()->with('collection', $collection);
          return view('questions.index',compact('questions', 'answers'));
    
  }

  public function show($id)
  {
    //Metoda wyciagajaca pojedynczy film z bazy danych
    $question = Question::findOrFail($id);
    $answers = Answer::with('score')->get();

    return view('questions.show', compact('question','answers'));
  }
}

