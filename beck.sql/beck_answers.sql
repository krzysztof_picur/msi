-- MySQL dump 10.13  Distrib 5.7.32, for Linux (x86_64)
--
-- Host: localhost    Database: beck
-- ------------------------------------------------------
-- Server version	5.7.32-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `answer_content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `question_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `answers_question_id_foreign` (`question_id`),
  CONSTRAINT `answers_question_id_foreign` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
INSERT INTO `answers` VALUES (1,'Nie jestem smutny ani przygnębiony',1,NULL,NULL),(2,'Odczuwam często smutek, przygnębienie',1,NULL,NULL),(3,'Przeżywam stale smutek, przygnębienie i nie mogę uwolnić się od tych przeżyć',1,NULL,NULL),(4,'Jestem stale tak smutny i nieszczęśliwy że jest to nie do wytrzymania',1,NULL,NULL),(5,'Nie przejmuję się zbytnio przeszłością',2,NULL,NULL),(6,'Często martwię się o przyszłosć',2,NULL,NULL),(7,'Obawiam się że w przyszłości nic dobergo mnie nie czeka',2,NULL,NULL),(8,'Czuję że przyszłość jest beznadziejna i nic tego nie zmieni',2,NULL,NULL),(9,'Sądzę, że nie popełniam większych zaniedbań',3,NULL,NULL),(10,'Sądzę, że czynię więcej zaniedbań niż inni',3,NULL,NULL),(11,'Kiedy spoglądam na to, co robiłem, widzę mnóstwo błędów i zaniedbań',3,NULL,NULL),(12,'Jestem zupełnie niewydolny i wszystko robię źle',3,NULL,NULL),(13,'Nie czuję się winnym ani wobec siebie, ani wobec innych.',5,NULL,NULL),(14,'Dość często miewam wyrzuty sumienia.',5,NULL,NULL),(15,'Często czuję, że zawiniłem.',5,NULL,NULL),(16,'Stale czuję się winny.',5,NULL,NULL),(17,'Sądzę, że nie zasługuję na karę',6,NULL,NULL),(18,'Sądzę, że zasługuję na karę',6,NULL,NULL),(19,'Spodziewam się ukarania',6,NULL,NULL),(20,'Wiem, że jestem karany (lub ukarany)',6,NULL,NULL),(21,'Jestem z siebie zadowolony',7,NULL,NULL),(22,'Nie jestem z siebie zadowolony',7,NULL,NULL),(23,'Czuję do siebie niechęć',7,NULL,NULL),(24,'Nienawidzę siebie',7,NULL,NULL),(25,'Nie czuję się gorszy od innych ludzi',8,NULL,NULL),(26,'Zarzucam sobie, że jestem nieudolny i popełniam błędy',8,NULL,NULL),(27,'Stale potępiam siebie za popełnione błędy',8,NULL,NULL),(28,'Winię siebie za wszelkie zło, które istnieje',8,NULL,NULL),(29,'Nie myślę o odebraniu sobie życia',9,NULL,NULL),(30,'Myślę o samobójstwie — ale nie mógłbym tego dokonać',9,NULL,NULL),(31,'Pragnę odebrać sobie życie',9,NULL,NULL),(32,'Popełnię samobójstwo, jak będzie odpowiednia sposobność',9,NULL,NULL),(33,'Nie płaczę częściej niż zwykle',10,NULL,NULL),(34,'Płaczę częściej niż dawniej',10,NULL,NULL),(35,'Ciągle chce mi się płakać',10,NULL,NULL),(36,'Chciałbym płakać, lecz nie jestem w stanie',10,NULL,NULL),(37,'Nie jestem bardziej podenerwowany niż dawniej',11,NULL,NULL),(38,'Jestem bardziej nerwowy i przykry niż dawniej',11,NULL,NULL),(39,'Jestem stale zdenerwowany lub rozdrażniony',11,NULL,NULL),(40,'Wszystko, co dawniej mnie drażniło, stało się obojętne',11,NULL,NULL),(41,'Ludzie interesują mnie jak dawniej',12,NULL,NULL),(42,'Interesuję się ludźmi mniej niż dawniej',12,NULL,NULL),(43,'Utraciłem większość zainteresowań innymi ludźmi',12,NULL,NULL),(44,'Utraciłem wszelkie zainteresowanie innymi ludźmi',12,NULL,NULL),(45,'Decyzje podejmuję łatwo, tak jak dawniej',13,NULL,NULL),(46,'Częściej niż kiedyś odwlekam podjęcie decyzji',13,NULL,NULL),(47,'Mam dużo trudności z podjęciem decyzji',13,NULL,NULL),(48,'Nie jestem w stanie podjąć żadnej decyzji',13,NULL,NULL),(49,'Sądzę, że wyglądam nie gorzej niż dawniej',14,NULL,NULL),(50,'Martwię się tym, że wyglądam staro i nieatrakcyjnie',14,NULL,NULL),(51,'Czuję, że wyglądam coraz gorzej',14,NULL,NULL),(52,'Jestem przekonany, że wyglądam okropnie i odpychająco',14,NULL,NULL),(53,'Mogę pracować jak dawniej',15,NULL,NULL),(54,'Z trudem rozpoczynam każdą czynność',15,NULL,NULL),(55,'Z wielkim wysiłkiem zmuszam się do zrobienia czegokolwiek',15,NULL,NULL),(56,'Nie jestem w stanie nic zrobić',15,NULL,NULL),(57,'Sypiam dobrze, jak zwykle',16,NULL,NULL),(58,'Sypiam gorzej niż dawniej',16,NULL,NULL),(59,'Rano budzę się 1–2 godziny za wcześnie i trudno jest mi ponownie usnąć',16,NULL,NULL),(60,'Budzę się kilka godzin za wcześnie i nie mogę usnąć',16,NULL,NULL),(61,'Nie męczę się bardziej niż dawniej',17,NULL,NULL),(62,'Męczę się znacznie łatwiej niż poprzednio.',17,NULL,NULL),(63,'Męczę się wszystkim, co robię.',17,NULL,NULL),(64,'Jestem zbyt zmęczony, aby cokolwiek robić.',17,NULL,NULL),(65,'Mam apetyt nie gorszy niż dawniej',18,NULL,NULL),(66,'Mam trochę gorszy apetyt',18,NULL,NULL),(67,'Apetyt mam wyraźnie gorszy',18,NULL,NULL),(68,'Nie mam w ogóle apetytu',18,NULL,NULL),(69,'Nie tracę na wadze (w okresie ostatniego miesiąca)',19,NULL,NULL),(70,'Straciłem na wadze więcej niż 2 kg',19,NULL,NULL),(71,'Straciłem na wadze więcej niż 4 kg',19,NULL,NULL),(72,'Straciłem na wadze więcej niż 6 kg',19,NULL,NULL),(73,'Nie martwię się o swoje zdrowie bardziej niż zawsze',20,NULL,NULL),(74,'Martwię się swoimi dolegliwościami, mam rozstrój żołądka, zaparcie, bóle',20,NULL,NULL),(75,'Stan mojego zdrowia bardzo mnie martwi, często o tym myślę',20,NULL,NULL),(76,'Tak bardzo martwię się o swoje zdrowie, że nie mogę o niczym innym myśleć',20,NULL,NULL),(77,'Moje zainteresowania seksualne nie uległy zmianom',21,NULL,NULL),(78,'Jestem mniej zainteresowany sprawami płci (seksu)',21,NULL,NULL),(79,'Problemy płciowe wyraźnie mniej mnie interesują',21,NULL,NULL),(80,'Utraciłem wszelkie zainteresowanie sprawami seksu',21,NULL,NULL),(81,'To, co robię, sprawia mi przyjemność.',4,NULL,NULL),(82,'Nie cieszy mnie to, co robię.',4,NULL,NULL),(83,'Nic mi teraz nie daje prawdziwego zadowolenia.',4,NULL,NULL),(84,'Nie potrafię przeżywać zadowolenia i przyjemności; wszystko mnie nuży.',4,NULL,NULL);
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-07  0:10:07
